<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
    <head>
        <title>Create Account</title>
        <link rel="stylesheet" type="text/css" href="resources/css/customstyle.css">
        <link rel="stylesheet" type="text/css" href="resources/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="resources/css/bootstrap-theme.css">
        <link rel="stylesheet" type="text/css" href="resources/css/bootstrap-datetimepicker.min.css">
        <link rel="stylesheet" type="text/css" href="resources/css/jquery-ui.css">
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css">
        <script src="resources/js/moment-with-locales.min.js"></script>
        <script src="resources/js/jquery-1.11.1.min.js"></script>
        <script src="resources/js/bootstrap-datetimepicker.min.js"></script>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
        <script src="resources/js/bootstrap.js"></script>
        <script>
            $( function() {
                $( "#birthday" ).datepicker({
                    dateFormat: 'yy-mm-dd'
                })
            } );
        </script>
    </head>
    <body>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="col-lg-1"></div>
            <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
                <div class="row-fluid">
                    <div class="col-lg-2 col-md-2 col-sm-3 hidden-xs"></div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-10">
                        <form action="${pageContext.request.contextPath}/createAccount" method="post">
                            <div class="panel panel-default">
                                <div class="panel-heading">Main info</div>
                                <div class="panel-body">
                                    <div class="row form-group">
                                        <div class="edit_label">Name:</div>
                                        <div class="edit_input"><input type="text" class="form-control input-sm" name="name" /></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="edit_label">Lastname:</div>
                                        <div class="edit_input"><input type="text" class="form-control input-sm" name="surName"/></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="edit_label">Middlename:</div>
                                        <div class="edit_input"><input type="text" class="form-control input-sm" name="middleName"/></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="edit_label">Birthday:</div>
                                        <div class="edit_input"><input type="text"class="form-control input-sm" name= "birthday" id="birthday"/></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="edit_label">Email:</div>
                                        <div class="edit_input"><input type="text" class="form-control input-sm" id="email" name="email"/></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="edit_label">Password:</div>
                                        <div class="edit_input"><input type="text" class="form-control input-sm" id="password" name="password"/></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="edit_label">Skype:</div>
                                        <div class="edit_input"><input type="text" class="form-control input-sm" id="skype" name="skype"/></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="edit_label">Icq:</div>
                                        <div class="edit_input"><input type="text" class="form-control input-sm" id="icq" name="icq"/></div>
                                    </div>
                                </div>
                                <ul class="list-group">
                                    <li class="text-center form-group list-group-item">
                                        <input class="btn btn-default btn-sm" type="submit" value="Save">
                                    </li>
                                </ul>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>