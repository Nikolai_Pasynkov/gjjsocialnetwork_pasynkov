<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <c:set var="message" value='${requestScope["message"]}' />
        <title>${message}</title>
    </head>
    <body>
        <p>${message}</p>
    </body>
</html>