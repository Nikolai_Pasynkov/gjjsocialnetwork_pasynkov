var curScale = 1;
var friendsOnPage = 3;
var pagesOnScale = 5;
var friendsOnScale = friendsOnPage * pagesOnScale;

function changeScale(count, side) {
    if (curScale == 1) {
        document.getElementById("chevron-left").style.display = "inline";
    }
    if (curScale * friendsOnScale >= count) {
        document.getElementById("chevron-right").style.display = "inline";
    }
    changeDisplay("none", count, curScale);
    if (side == 1) {
        curScale = curScale + 1;
    } else {
        curScale = curScale - 1;
    }
    if (curScale == 1) {
        document.getElementById("chevron-left").style.display = "none";
    }
    if (curScale * friendsOnScale >= count) {
        document.getElementById("chevron-right").style.display = "none";
    }
    changeDisplay("inline", count, curScale);
}

function changeDisplay(display, count, curScale) {
    var maxNumberOnScale = curScale * pagesOnScale;
    if (curScale * friendsOnScale > count) {
        maxNumberOnScale = count / friendsOnPage + 1;
    }
    for (var i = (curScale - 1) * pagesOnScale + 1; i <= maxNumberOnScale; i = i + 1) {
        document.getElementById("chngPg" + i).style.display = display;
    }
}

var cur = 1;

function changePage(newPage, count) {
    if(newPage != cur){
        for (var i = friendsOnPage * (newPage - 1); i < friendsOnPage * newPage; i = i + 1) {
            if (i < count) {
                document.getElementById("friend" + i + ".wrapper").style.display = 'block';
            }
        }
        for (var i = friendsOnPage * (cur - 1); i < friendsOnPage * cur; i = i + 1) {
            if (i < count) {
                document.getElementById("friend" + i + ".wrapper").style.display = 'none';
            }
        }
        cur = newPage;
    }
}