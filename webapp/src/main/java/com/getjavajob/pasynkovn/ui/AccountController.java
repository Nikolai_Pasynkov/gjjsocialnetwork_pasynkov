package com.getjavajob.pasynkovn.ui;

import com.getjavajob.pasynkovn.common.account.*;
import com.getjavajob.pasynkovn.service.AccountService;
import com.getjavajob.pasynkovn.service.ServiceException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.List;

@Controller
public class AccountController {

    private final AccountService accountService;

    private final ServletContext servletContext;

    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

    @Autowired
    public AccountController(AccountService accountService, ServletContext servletContext) {
        this.accountService = accountService;
        this.servletContext = servletContext;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView loginGet(HttpSession session) {
        logger.info("Start: start page");
        ModelAndView modelAndView;
        Object o = session.getAttribute("login");
        if (o != null && (Boolean) o) {
            logger.info("Start page: already login");
            Account account = accountService.read(Integer.valueOf((String) session.getAttribute("id")));
            modelAndView = new ModelAndView("showAccount");
            modelAndView.addObject("account", account);
        } else {
            logger.info("Start page: not login");
            modelAndView = new ModelAndView("startPage");
        }
        logger.info("Finish: start page");
        return modelAndView;
    }

    @RequestMapping(value = "/showAccount", method = RequestMethod.POST)
    public ModelAndView login(@RequestParam("email") String email, @RequestParam("password") String password,
                              HttpServletResponse resp, HttpServletRequest req, HttpSession session) {
        logger.info("Start: show account: login");
        ModelAndView modelAndView;
        Account account = accountService.getByEmail(email, password);
        if (account == null) {
            logger.warn("Warning: show account: login. No such email-password");
            modelAndView = new ModelAndView("startPage");
            modelAndView.addObject("message", "Invalid email-password");
        } else {
            logger.info("Success: show account: login");
            account = accountService.read(account.getId());
            modelAndView = new ModelAndView("showAccount");
            modelAndView.addObject("account", account);
            if (req.getParameter("remember") != null) {
                Cookie cookie = new Cookie("email", email);
                resp.addCookie(cookie);
                cookie = new Cookie("password", password);
                resp.addCookie(cookie);
                cookie = new Cookie("id", account.getId() + "");
                resp.addCookie(cookie);
            }
            session.setAttribute("id", account.getId() + "");
            session.setAttribute("login", true);
            session.setAttribute("name", account.getName());
            session.setAttribute("surName", account.getSurName());
        }
        logger.info("Finish: show account: login");
        return modelAndView;
    }

    @RequestMapping(value = "/logout")
    public ModelAndView logout(HttpSession session, HttpServletResponse resp) {
        logger.info("Start: logout");
        session.invalidate();
        Cookie cookie = new Cookie("email", "");
        cookie.setMaxAge(0);
        resp.addCookie(cookie);
        cookie = new Cookie("password", "");
        cookie.setMaxAge(0);
        resp.addCookie(cookie);
        cookie = new Cookie("id", "");
        cookie.setMaxAge(0);
        resp.addCookie(cookie);
        logger.info("Finish: logout");
        return new ModelAndView("startPage");
    }

    @RequestMapping(value = "/showAccount", method = RequestMethod.GET)
    public ModelAndView showAccount(@RequestParam("id") int id,  HttpSession session) {
        logger.info("Start: show account");
        Account account = accountService.read(id);
        ModelAndView modelAndView;
        if (account == null) {
            logger.warn("Warning: show account. Account is null");
            modelAndView = new ModelAndView("fail");
            modelAndView.addObject("message", "Error: no account with such id");
            return modelAndView;
        } else {
            modelAndView = new ModelAndView("showAccount");
            modelAndView.addObject("account", account);
            Account me = accountService.read(Integer.valueOf((String) session.getAttribute("id")));
            modelAndView.addObject("isFriend", me.haveFriend(account));
            logger.info("Finish: show account");
            return modelAndView;
        }
    }

    @RequestMapping(value = "/showFriends", method = RequestMethod.GET)
    public ModelAndView showFriends(@RequestParam("id") int id) {
        logger.info("Start: show friends");
        Account account = accountService.read(id);
        ModelAndView modelAndView = new ModelAndView("showFriends");
        modelAndView.addObject("account", account);
        logger.info("Finish: show friends");
        return modelAndView;
    }

    @RequestMapping(value = "/createAccount", method = RequestMethod.GET)
    public ModelAndView createAccountGet() {
        return new ModelAndView("createAccount");
    }

    @RequestMapping(value = "/createAccount", method = RequestMethod.POST)
    public ModelAndView createAccountPost(@ModelAttribute Account account) {
        logger.info("Start: create account");
        account.setRole(Role.valueOf("User"));
        ModelAndView modelAndView;
        try {
            accountService.create(account);
        } catch (ServiceException e) {
            logger.error("Create account: fail to create");
            modelAndView = new ModelAndView("fail");
            modelAndView.addObject("message", "Error: fail to create account");
            return modelAndView;
        }
        modelAndView = new ModelAndView("startPage");
        logger.info("Finish: create account");
        return modelAndView;
    }

    @RequestMapping(value = "/updateAccount", method = RequestMethod.GET)
    public ModelAndView updateAccountGet(@RequestParam("id") int id, HttpSession session) {
        logger.info("Start: update account");
        ModelAndView modelAndView;
        if (id == Integer.valueOf((String) session.getAttribute("id"))) {
            Account account = accountService.read(id);
            modelAndView = new ModelAndView("updateAccount");
            modelAndView.addObject("account", account);
            logger.info("Update account: valid access");
            return modelAndView;
        } else {
            modelAndView = new ModelAndView("fail");
            modelAndView.addObject("message", "Error: not your account to edit.");
            logger.warn("Update account: account_id=" + session.getAttribute("id") + " try to edit account_id=" + id);
            return modelAndView;
        }
    }

    @RequestMapping(value = "/updateAccount", method = RequestMethod.POST)
    public void updateAccountPost(@ModelAttribute Account account, HttpServletRequest req, HttpServletResponse resp, HttpSession session) throws ServletException, IOException {
        account.setRole(Role.valueOf("User"));
        if (account.getPhones() != null) {
            logger.info("Update account: edit phones");
            for (Phone p : account.getPhones()) {
                p.setAccount(account);
            }
        }
        try {
            account.setFriends(accountService.read(account.getId()).getFriends());
            accountService.update(account);
        } catch (ServiceException e) {
            logger.info("Update account: fail to update account");
            req.setAttribute("message", "Error: fail to update account. " + e.getMessage());
            req.getRequestDispatcher("/WEB-INF/jsp/fail.jsp").forward(req, resp);
        }
        for (Cookie cookie : req.getCookies()) {
            if ("id".equals(cookie.getName())) {
                cookie = new Cookie("email", account.getEmail());
                resp.addCookie(cookie);
                cookie = new Cookie("password", account.getPassword());
                resp.addCookie(cookie);
            }
        }
        session.setAttribute("name", account.getName());
        session.setAttribute("surName", account.getSurName());
        logger.info("Finish: update account");
        resp.sendRedirect(req.getContextPath() + "/showAccount?id=" + account.getId());
    }

    @RequestMapping("/getAccounts")
    @ResponseBody
    public List<Account> getAccounts(@RequestParam("filter") final String filter) {
        List<Account> accountList = accountService.readAll();
        CollectionUtils.filter(accountList, new Predicate<Account>() {
            @Override
            public boolean evaluate(Account account) {
                return account.getName().toLowerCase().contains(filter.toLowerCase()) || account.getSurName().toLowerCase().contains(filter.toLowerCase());
            }
        });
        return accountList;
    }

    @RequestMapping(value = "/uploadImage", method = RequestMethod.POST)
    public String uploadPhoto(@RequestParam("id") int id, @RequestPart("file") MultipartFile filePart, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("Start: upload photo");
        try {
            if (!filePart.isEmpty()) {
                byte[] photo = filePart.getBytes();
                Photo photo1 = new Photo();
                photo1.setId(id);
                photo1.setBytes(photo);
                accountService.addOrUpdate(photo1);
            }
            logger.info("Finish: upload photo");
            return "redirect:/showAccount?id=" + id;
        } catch (Exception e) {
            logger.info("Upload photo: fail to upload photo");
            req.setAttribute("message", "Error: fail to upload photo. " + e.getMessage());
            req.getRequestDispatcher("/WEB-INF/jsp/fail.jsp").forward(req, resp);
        }
        return "";
    }

    @RequestMapping(value = "/imageDisplay", method = RequestMethod.GET)
    public void showImage(@RequestParam("id") int id, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
        byte[] photoByte;
        Photo photo = accountService.getPhoto(id);
        if (photo == null) {
            photoByte = IOUtils.toByteArray(servletContext.getResourceAsStream("/resources/img/defaultPhoto.JPG"));
        } else {
            photoByte = photo.getBytes();
        }
        response.getOutputStream().write(photoByte);
        response.getOutputStream().close();
    }

    @RequestMapping(value = "/addFriend", method = RequestMethod.GET)
    public void addFriend(@RequestParam("id") int id, @RequestParam("friend") int friend, HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Account account = accountService.read(id);
        System.out.println(id);
        System.out.println(account.getName());
        account.getFriends().add(accountService.read(friend));
        System.out.println(friend);
        System.out.println(accountService.read(friend).getName());
        try {
            accountService.update(account);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        resp.sendRedirect(req.getContextPath() + "/showAccount?id=" + friend);
    }

    @InitBinder
    public void initBinder(WebDataBinder binder){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }
}
