package com.getjavajob.pasynkovn.ui.filters;

import com.getjavajob.pasynkovn.common.account.Account;
import com.getjavajob.pasynkovn.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class LoginFilter implements Filter {

    private final AccountService accountService;

    @Autowired
    public LoginFilter(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        System.out.println("log filter");
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        Object o = httpServletRequest.getSession().getAttribute("login");
        if (o != null && (Boolean) o) {
            filterChain.doFilter(httpServletRequest, servletResponse);
        } else {
            Cookie[] cookies = httpServletRequest.getCookies();
            String id = null;
            for (Cookie cookie : cookies) {
                if ("id".equals(cookie.getName())) {
                    id = cookie.getValue();
                    httpServletRequest.getSession().setAttribute("id", id);
                }
            }
            if (id == null) {
                httpServletRequest.setAttribute("message", "Invalid login");
                httpServletRequest.getRequestDispatcher("/WEB-INF/jsp/startPage.jsp").forward(servletRequest, servletResponse);
            } else {
                Account account = accountService.read(Integer.valueOf((String) httpServletRequest.getSession().getAttribute("id")));
                httpServletRequest.getSession().setAttribute("name", account.getName());
                httpServletRequest.getSession().setAttribute("surName", account.getSurName());
                httpServletRequest.getSession().setAttribute("login", true);
                filterChain.doFilter(httpServletRequest, servletResponse);
            }
        }
    }

    @Override
    public void destroy() {
    }
}
