<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html lang="eng">
<head>
    <title>My page</title>
    <link rel="stylesheet" type="text/css" href="resources/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="resources/css/bootstrap-theme.css">
    <link rel="stylesheet" type="text/css" href="resources/css/jquery-ui.css">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
    <script src="resources/js/bootstrap.js"></script>
    <script src="resources/js/jquery.bootpag.js"></script>
    <script src="resources/js/pagination.js"></script>
    <script>
        $(document).ready
        ( function() {
            $( "#AccountName" ).autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "${pageContext.request.contextPath}/getAccounts",
                        data: {
                            filter: request.term
                        },
                        success: function(data){
                            response($.map( data, function (account, i) {
                                return {value: account.id, label: account.name + ' ' + account.surName}
                            }));
                        }
                    });
                },
                minLength: 2,
                select: function (event, ui) {
                    location.href = '${pageContext.request.contextPath}/showAccount?id='+ui.item.value;
                },
                open: function () {
                    $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
                },
                close: function () {
                    $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
                }
            });
        } );
    </script>

</head>
<body>
<!-- Навигация -->
<nav class="navbar navbar-default " role="navigation">
    <div class="navbar-header">
        <ul class="nav navbar-nav ">
            <li class="menu-item dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="glyphicon glyphicon-user"></i> ${sessionScope["name"]} ${sessionScope["surName"]} <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="${pageContext.request.contextPath}/showAccount?id=${id}">My Page</a></li>
                    <li><a href="${pageContext.request.contextPath}/updateAccount?id=${id}">Edit</a></li>
                    <li class="divider"></li>
                    <li><a href="${pageContext.request.contextPath}/logout">Logout</a></li>
                </ul>
            </li>
            <li>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
                    <span class="sr-only">Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </li>
        </ul>
    </div>

    <div class="collapse navbar-collapse" id="menu">
        <ul class="nav navbar-nav">
            <%--<li><a href="#">Groups</a></li>--%>
            <%--<li><a href="#">Messages</a></li>--%>
            <li><a href="${pageContext.request.contextPath}/showFriends?id=${id}">Friends</a></li>
            <li>
                <form class="navbar-form" role="search">
                    <div class="ui-widget">
                        <input type="text" class="form-control" id="AccountName" placeholder="Search...">
                    </div>
                </form>
            </li>
        </ul>
    </div>
</nav>

<div class="container-fluid">
    <div class="row-fluid">
        <div class="col-lg-2 col-md-2"></div>
        <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
            <!--Friends Table-->
            <p>My Friends</p>
            <c:set var="friends" value="${account.friends}"/>
            <c:set var="count" value="${friends.size()}" scope="page" />
            <c:set var="cur" value="1" scope="page" />

            <c:forEach items="${friends}" varStatus="loop">
                <c:choose>
                    <c:when test="${loop.index eq 3*cur-3 || loop.index eq 3*cur - 2 || loop.index eq 3*cur - 1}">
                        <div id="friend${loop.index}.wrapper" style="display: block;">
                    </c:when>
                    <c:otherwise>
                        <div id="friend${loop.index}.wrapper" style="display: none;">
                    </c:otherwise>
                </c:choose>
                            <p>
                                <img src="${pageContext.request.contextPath}/imageDisplay?id=${friends[loop.index].id}" class="img-rounded" width="50" height="50">
                                <a href="${pageContext.request.contextPath}/showAccount?id=${friends[loop.index].id}"> ${friends[loop.index].name} ${friends[loop.index].surName}</a>
                            </p>
                        </div>
            </c:forEach>

                <ul id="pagination" class="pagination bootpag">
                    <c:choose>
                        <c:when test="${count > 5}">
                            <li id="chevron-left" style="display: none"><a href="#" onclick="changeScale(${count}, 0);"><i class="glyphicon glyphicon-chevron-left"></i></a></li>
                            <c:forEach var="i" begin="1" end="5">
                                <li id="chngPg${i}" style="display: inline"><a href="#" onclick="changePage(${i}, ${count});">${i}</a></li>
                            </c:forEach>
                            <c:forEach var="i" begin="6" end="${count/3 + 1}">
                                <li id="chngPg${i}" style="display: none"><a href="#" onclick="changePage(${i}, ${count});">${i}</a></li>
                            </c:forEach>
                            <li id="chevron-right" style="display: inline"><a href="#" onclick="changeScale(${count}, 1);"><i class="glyphicon glyphicon-chevron-right"></i></a></li>
                        </c:when>
                        <c:otherwise>
                            <c:forEach var="i" begin="1" end="${count/3 + 1}">
                                <li><a href="#" onclick="changePage(${i}, ${count});">${i}</a></li>
                            </c:forEach>
                        </c:otherwise>
                    </c:choose>
                </ul>
            </div>
        </div>
    </div>
</div>
</body>
</html>
