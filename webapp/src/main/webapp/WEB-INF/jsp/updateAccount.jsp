<%@page contentType="text/html; charset=utf-8" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--<!DOCTYPE html>--%>
<html lang="eng">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Edit account</title>
    <link rel="stylesheet" type="text/css" href="resources/css/customstyle.css">
    <link rel="stylesheet" type="text/css" href="resources/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="resources/css/bootstrap-theme.css">
    <link rel="stylesheet" type="text/css" href="resources/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" type="text/css" href="resources/css/jquery-ui.css">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css">
    <script src="resources/js/moment-with-locales.min.js"></script>
    <script src="resources/js/jquery-1.11.1.min.js"></script>
    <script src="resources/js/bootstrap-datetimepicker.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
    <script src="resources/js/bootstrap.js"></script>
    <script src="resources/js/pagination.js"></script>
    <script>
        $(document).ready
        ( function() {
            $( "#AccountName" ).autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "${pageContext.request.contextPath}/getAccounts",
                        data: {
                            filter: request.term
                        },
                        success: function(data){
                            response($.map( data, function (account, i) {
                                return {value: account.id, label: account.name + ' ' + account.surName}
                            }));
                        }
                    });
                },
                minLength: 2,
                select: function (event, ui) {
                    console.log(ui.item ? "Selected: " + ui.item.label : "Nothing selected");
                    location.href = '${pageContext.request.contextPath}/showAccount?id='+ui.item.value;
                },
                open: function () {
                    $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
                },
                close: function () {
                    $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
                }
            });
        } );
    </script>
    <script>
        $( function() {
            $( "#birthday" ).datepicker({
                dateFormat: 'yy-mm-dd'
            })
        } );
    </script>
    <script type="text/javascript">
        $(function() {

            // Start indexing at the size of the current list
            var index = ${fn:length(account.phones)};

            // Add a new Phone
            $("#add").off("click").on("click", function() {
                $(this).before(function() {
                    var html = '<div id="phones' + index + '.wrapper">';
                    html += '<input class="form-control input-sm" type="text" id="phones' + index + '.phone" name="phones[' + index + '].phone" />';
                    html += '<input type="hidden" id="phones' + index + '.remove" name="phones[' + index + '].remove" value="0" />';
                    html += '<a href="#" class="phones.remove" data-index="' + index + '">remove</a>';
                    html += "</div>";
                    return html;
                });
                $("#phones" + index + "\\.wrapper").show();
                index++;
                return false;
            });

            // Remove Phone
            $("a.phones\\.remove").off("click").on("click", function() {
                var index2remove = $(this).data("index");
                $("#phones" + index2remove + "\\.wrapper").hide();
                $("#phones" + index2remove + "\\.remove").val("1");
                return false;
            });

        });
    </script>
</head>
<body>
<!-- Навигация -->
<nav class="navbar navbar-default " role="navigation">
    <div class="navbar-header">
        <ul class="nav navbar-nav ">
            <li class="menu-item dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="glyphicon glyphicon-user"></i> ${sessionScope["name"]} ${sessionScope["surName"]} <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="${pageContext.request.contextPath}/showAccount?id=${id}">My Page</a></li>
                    <li><a href="${pageContext.request.contextPath}/updateAccount?id=${id}">Edit</a></li>
                    <li class="divider"></li>
                    <li><a href="${pageContext.request.contextPath}/logout">Logout</a></li>
                </ul>
            </li>
        </ul>
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
            <span class="sr-only">Navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <div class="collapse navbar-collapse" id="menu">
        <ul class="nav navbar-nav">
            <li><a href="${pageContext.request.contextPath}/showFriends?id=${id}">Friends</a></li>
            <li>
                <form class="navbar-form" role="search">
                    <div class="ui-widget">
                        <input type="text" class="form-control" id="AccountName" placeholder="Search...">
                    </div>
                </form>
            </li>
        </ul>
    </div>

</nav>
<%--Update Account Page--%>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="col-lg-1"></div>
        <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
            <!--Body content-->
            <div class="row-fluid">
                <!-- photo -->
                <div class="col-lg-2 col-md-2 col-sm-3 hidden-xs text-center">
                    <img src="${pageContext.request.contextPath}/imageDisplay?id=${account.id}" class="img-rounded" width="150" height="150" vspace="5">
                    <form action="${pageContext.request.contextPath}/uploadImage?id=${account.id}" method="post" enctype="multipart/form-data">
                        <div class="fileUpload btn btn-default btn-sm">
                            <span>Upload</span>
                            <input type="file" class="upload" name="file" />
                        </div>
                        <input type="submit" class="btn btn-default btn-sm" value="Ok"/>
                    </form>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-10">
                    <form:form action="${pageContext.request.contextPath}/updateAccount?id=${account.id}" modelAttribute="account" method="post" name="account">
                        <div class="panel panel-default">
                            <div class="panel-heading">Main info</div>
                            <div class="panel-body">
                                <div class="row form-group">
                                    <div class="edit_label">Name:</div>
                                    <div class="edit_input"><form:input path="name" type="text" class="form-control input-sm" name="name" value="${account.name}" /></div>
                                </div>
                                <div class="row form-group">
                                    <div class="edit_label">Lastname:</div>
                                    <div class="edit_input"><form:input path="surName" type="text" class="form-control input-sm" name="surName" value="${account.surName}"/></div>
                                </div>
                                <div class="row form-group">
                                    <div class="edit_label">Middlename:</div>
                                    <div class="edit_input"><form:input path="middleName" type="text" class="form-control input-sm" name="middleName" value="${account.middleName}"/></div>
                                </div>
                                <div class="row form-group">
                                    <div class="edit_label">Birthday:</div>
                                    <fmt:formatDate var="birthdayString" value='${account.birthday}' pattern='yyyy-MM-dd' />
                                    <div class="edit_input"><form:input type="text" path="birthday" class="form-control input-sm" name= "birthday" id="birthday" value ='${birthdayString}'/></div>
                                </div>
                                <div class="row form-group">
                                    <div class="edit_label">Email:</div>
                                    <div class="edit_input"><form:input path="email" type="text" class="form-control input-sm" id="email" name="email" value="${account.email}" /></div>
                                </div>
                                <div class="row form-group">
                                    <div class="edit_label">Skype:</div>
                                    <div class="edit_input"><form:input path="skype" type="text" class="form-control input-sm" id="skype" name="skype" value="${account.skype}" /></div>
                                </div>
                                <div class="row form-group">
                                    <div class="edit_label">Icq:</div>
                                    <div class="edit_input"><form:input path="icq" type="text" class="form-control input-sm" id="icq" name="icq" value="${account.icq}" /></div>
                                </div>
                                <div class="row form-group">
                                    <div class="edit_label">Phones:</div>
                                    <div class="edit_input">
                                        <c:set var="count" value="0" scope="page" />
                                        <c:forEach items="${account.phones}" varStatus="loop">
                                        <c:choose>
                                        <c:when test="${account.phones[loop.index].remove eq 1}">
                                        <div id="phones${loop.index}.wrapper" class="hidden">
                                            </c:when>
                                            <c:otherwise>
                                            <div id="phones${loop.index}.wrapper">
                                                </c:otherwise>
                                                </c:choose>
                                                <form:input class="form-control input-sm" path="phones[${loop.index}].phone" value="${account.phones[loop.index].phone}" readonly="true"/>
                                                <c:choose>
                                                    <c:when test="${phones[loop.index].remove eq 1}"><c:set var="hiddenValue" value="1" /></c:when>
                                                    <c:otherwise><c:set var="hiddenValue" value="0" /></c:otherwise>
                                                </c:choose>
                                                <form:hidden path="phones[${loop.index}].remove" value="${hiddenValue}" />
                                                <a href="#" class="phones.remove" data-index="${loop.index}">remove</a>
                                                <c:set var="count" value="${count + 1}" scope="page"/>
                                            </div>
                                            </c:forEach>
                                            <p id="message" class="warning"></p>
                                            <button id="add" type=button class="btn btn-default btn-sm">Add</button>
                                        </div>
                                    </div>
                                </div>
                                <ul class="list-group">
                                    <li class="text-center form-group list-group-item">
                                        <input class="btn btn-default btn-sm" type="submit" value="Save">
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <form:input path="id" type="hidden" name="id" value="${account.id}"/>
                        <form:input path="role" type="hidden" name="role" value="${account.role}"/>
                        <form:input path="password" type="hidden" name="password" value="${account.password}"/>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>