<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html lang="eng">
<head>
    <title>My page</title>
    <link rel="stylesheet" type="text/css" href="resources/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="resources/css/bootstrap-theme.css">
    <link rel="stylesheet" type="text/css" href="resources/css/jquery-ui.css">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
    <script src="resources/js/bootstrap.js"></script>
    <script src="resources/js/pagination.js"></script>
    <script>
        $(document).ready
        ( function() {
            $( "#AccountName" ).autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "${pageContext.request.contextPath}/getAccounts",
                        data: {
                            filter: request.term
                        },
                        success: function(data){
                            response($.map( data, function (account, i) {
                                return {value: account.id, label: account.name + ' ' + account.surName}
                            }));
                        }
                    });
                },
                minLength: 2,
                select: function (event, ui) {
                    location.href = '${pageContext.request.contextPath}/showAccount?id='+ui.item.value;
                },
                open: function () {
                    $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
                },
                close: function () {
                    $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
                }
            });
        } );
    </script>
</head>

<body>
<!-- Навигация -->
<nav class="navbar navbar-default " role="navigation">
    <div class="navbar-header">
        <ul class="nav navbar-nav ">
            <li class="menu-item dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="glyphicon glyphicon-user"></i> ${sessionScope["name"]} ${sessionScope["surName"]} <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="${pageContext.request.contextPath}/showAccount?id=${id}">My Page</a></li>
                    <li><a href="${pageContext.request.contextPath}/updateAccount?id=${id}">Edit</a></li>
                    <li class="divider"></li>
                    <li><a href="${pageContext.request.contextPath}/logout">Logout</a></li>
                </ul>
            </li>
            <li>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
                    <span class="sr-only">Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </li>
        </ul>
    </div>
    <div class="collapse navbar-collapse" id="menu">
        <ul class="nav navbar-nav">
            <%--<li><a href="#">Groups</a></li>--%>
            <%--<li><a href="#">Messages</a></li>--%>
            <li><a href="${pageContext.request.contextPath}/showFriends?id=${id}">Friends</a></li>
            <li>
                <form class="navbar-form" role="search">
                    <div class="ui-widget">
                        <input type="text" class="form-control" id="AccountName" placeholder="Search...">
                    </div>
                </form>
            </li>
        </ul>
    </div>

</nav>
<!-- Окно Аккаунта -->
<div class="container-fluid">
    <div class="row-fluid">
        <div class="col-lg-1"></div>
        <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
            <!--Body content-->
            <div class="row-fluid">
                <div class="col-lg-2 col-md-2 col-sm-3 hidden-xs">
                    <!-- photo -->
                    <img src="${pageContext.request.contextPath}/imageDisplay?id=${account.id}" class="img-rounded" width="150" height="150" vspace="5">
                    <c:set var="idViewer" value='${sessionScope["id"]}' />
                    <c:set var="idToView" value='${account.id}' />
                    <c:choose>
                        <c:when test="${idViewer==idToView}">
                            <div class="text-center">
                                <a href="${pageContext.request.contextPath}/updateAccount?id=${account.id}" class="btn btn-default btn-sm" role="button">Edit account</a>
                            </div>
                        </c:when>
                        <%--<c:otherwise>--%>
                            <%--<div class="text-center">--%>
                                <%--<a href="${pageContext.request.contextPath}/addFriend?id=${account.id}&friend=${idToView}" class="btn btn-default btn-sm" role="button">Add friend</a>--%>
                            <%--</div>--%>
                        <%--</c:otherwise>--%>
                    </c:choose>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <!-- Account info -->
                    <div class="panel panel-default">
                        <!-- Default panel contents -->
                        <div class="panel-heading">Main info</div>
                        <div class="panel-body">
                            ${account.name} ${account.surName}
                        </div>
                        <table class="table">
                            <tr>
                                <td>Birthday</td>
                                <fmt:formatDate var="birthdayString" value='${account.birthday}' pattern='yyyy-MM-dd' />
                                <td>${birthdayString}</td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td>${account.email}</td>
                            </tr>
                            <tr>
                                <td>Skype</td>
                                <td>${account.skype} </td>
                            </tr>
                            <tr>
                                <td>Icq</td>
                                <td>${account.icq}</td>
                            </tr>
                            <tr>
                                <td>Phones</td>
                                <td>
                                    <c:forEach var="phone" items="${account.phones}">
                                        <p>${phone.phone}</p>
                                    </c:forEach>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>