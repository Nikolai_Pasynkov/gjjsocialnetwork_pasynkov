<%@page contentType="text/html; charset=utf-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Start page</title>
    <link rel="stylesheet" type="text/css" href="resources/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="resources/css/bootstrap-theme.css">
</head>
<body>
<div class="container-fluid">
    <div class="row"></div>
    <div class="row-fluid">
        <div class="col-lg-4 col-md-4 col-sm-3 col-xs-2"></div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-8">
            <div class="panel  panel-info">
                <div class="panel-heading">
                    <h3 class="text-center">Welcome</h3>
                </div>

                <div class="panel-body text-center">
                    <c:set var="message" value='${requestScope["message"]}' />
                    <c:if test="'Invalid email-password'=${message}">
                        <p><em>Invalid email - пароль</em></p>
                    </c:if>
                    <h5>Please log in to existing account. If you don't have account you can <a href="${pageContext.request.contextPath}/createAccount">sign up</a></h5>
                    <form action="${pageContext.request.contextPath}/showAccount" method="post">
                        <label for="email">Email</label><br>
                        <input type="text" class="form-control" id="email" name="email">

                        <label for="password">Password</label><br>
                        <input type="text" class="form-control"  id="password" name="password">
                        <label for="remember">Remember?</label>
                        <input type="checkbox" name="remember" id="remember" checked><br>

                        <input type="submit" class="btn btn-default" value="Sign in">
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-3 col-xs-2"></div>
    </div>
</div>
</body>
</html>