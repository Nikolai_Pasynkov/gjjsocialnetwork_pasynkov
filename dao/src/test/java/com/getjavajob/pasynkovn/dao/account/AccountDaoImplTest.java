package com.getjavajob.pasynkovn.dao.account;

import com.getjavajob.pasynkovn.common.account.Account;
import com.getjavajob.pasynkovn.common.account.Role;
import com.getjavajob.pasynkovn.dao.exceptions.DAOException;
import org.h2.tools.RunScript;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.io.FileReader;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-context.xml", "classpath:dao-context-overrides.xml"})
public class AccountDaoImplTest {

    @Autowired
    private AccountDao accountDao;

    @Autowired
    DataSource dbDataSource;
    /**
     * Create table "accounts" with one row
     * */
    @Before
    public void init() {
        try (Connection connection = dbDataSource.getConnection()) {
            RunScript.execute(connection, new FileReader("src\\test\\resources\\script\\create.sql"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Destroy table "accounts" with one row
     * */
    @After
    public void destroy() {
        try (Connection connection = dbDataSource.getConnection()) {
            RunScript.execute(connection, new FileReader("src\\test\\resources\\script\\destroy.sql"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAdd() {
        Account newAcc = new Account();
        newAcc.setPassword("qwerty");
        newAcc.setName("Ivan");
        newAcc.setSurName("Ivanov");
        newAcc.setEmail("Ivanov@mail.ru");
        newAcc.setRole(Role.valueOf("User"));
        try {
            accountDao.add(newAcc);
            newAcc.setId(124);
            assertEquals(newAcc, accountDao.getById(124));
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetById() {
        Account accActual = null;
        try {
            accActual = accountDao.getById(123);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        Account accExpected = new Account();
        accExpected.setId(123);
        accExpected.setName("Bob");
        accExpected.setSurName("Fisher");
        accExpected.setMiddleName(null);
        accExpected.setEmail("bobfisher@mail.com");
        accExpected.setIcq("123456789");
        accExpected.setSkype("asdasdasd");
        accExpected.setRole(Role.valueOf("User"));
        accExpected.setPassword("qwerty123");
        assertEquals(accExpected, accActual);
    }

    @Test
    public void testUpdate() throws Exception {
        Account accExpected = new Account();
        accExpected.setId(123);
        accExpected.setName("Ivan");
        accExpected.setSurName("Ivanov");
        accExpected.setMiddleName("Petrovich");
        accExpected.setEmail("Ivanov@mail.ru");
        accExpected.setIcq("555555");
        accExpected.setSkype("Ivanov123");
        accExpected.setRole(Role.valueOf("User"));
        accExpected.setPassword("qwerty");
        accountDao.update(accExpected);
        Account accActual = accountDao.getById(123);
        assertEquals(accExpected, accActual);
    }

    @Test
    public void testDelete() throws Exception {
        assertEquals(1, accountDao.getAll().size());
        accountDao.delete(123);
        assertEquals(0, accountDao.getAll().size());
    }

    @Test
    public void testGetAll() throws Exception {
        List<Account> expAccounts = new ArrayList<>();
        Account accExpected = new Account();
        accExpected.setId(123);
        accExpected.setName("Bob");
        accExpected.setSurName("Fisher");
        accExpected.setMiddleName(null);
        accExpected.setEmail("bobfisher@mail.com");
        accExpected.setIcq("123456789");
        accExpected.setSkype("asdasdasd");
        accExpected.setRole(Role.valueOf("User"));
        accExpected.setPassword("qwerty123");
        expAccounts.add(accExpected);
        assertEquals(expAccounts, accountDao.getAll());
        assertEquals(accountDao.getAll().get(0).getId(), 123);
    }

}