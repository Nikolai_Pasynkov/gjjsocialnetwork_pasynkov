package com.getjavajob.pasynkovn.dao.exceptions;

public class DAOException extends Exception {
    private static final long serialVersionUID = 1L;

    public DAOException(String msg) {
        super(msg);
    }
}

