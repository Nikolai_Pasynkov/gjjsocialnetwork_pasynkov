package com.getjavajob.pasynkovn.dao.account;

import com.getjavajob.pasynkovn.common.account.Account;
import com.getjavajob.pasynkovn.dao.exceptions.DAOException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.*;

@Repository
public class AccountDaoImpl implements AccountDao {

    private EntityManager entityManager;

    @PersistenceContext(type = PersistenceContextType.EXTENDED)
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(Account account) throws DAOException {
        merge(account);
    }

    private void merge(Account account) throws DAOException {
        try {
            entityManager.merge(account);
        } catch (Exception e) {
            throw new DAOException(e.getMessage());
        }
    }

    @Override
    public Account getById(long id) throws DAOException {
        Account account;
        try {
            account = entityManager.find(Account.class, id);
        } catch (Exception e) {
            throw new DAOException(e.getMessage());
        }
        return account;
    }

    @Override
    public Account getByEmailAndPassword(String email, String password) throws DAOException {
        Account account;
        try {
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<Object> criteriaQuery = cb.createQuery();
            Root<Account> from = criteriaQuery.from(Account.class);
            CriteriaQuery<Object> select = criteriaQuery.select(from).where(cb.equal(from.get("email"), email), cb.equal(from.get("password"), password));
            TypedQuery<Object> typedQuery = entityManager.createQuery(select);
            List<Object> resultList = typedQuery.getResultList();
            account = resultList.isEmpty() ? null : (Account) resultList.get(0);
        } catch (Exception e) {
            throw new DAOException(e.getMessage());
        }
        return account;
    }

    @Override
    public void update(Account account) throws DAOException {
        merge(account);
    }

    @Override
    public void delete(long id) throws DAOException {
        try {
            entityManager.remove(getById(id));
        } catch (Exception e) {
            throw new DAOException(e.getMessage());
        }
    }

    public List<Account> getAll() throws DAOException {
        try {
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<Object> criteriaQuery = cb.createQuery();
            Root<Account> from = criteriaQuery.from(Account.class);
            CriteriaQuery<Object> select = criteriaQuery.select(from);
            TypedQuery<Object> typedQuery = entityManager.createQuery(select);
            List<Object> resultList = typedQuery.getResultList();
            List<Account> accountList = new ArrayList<>();
            for (Object o : resultList) {
                accountList.add((Account) o);
            }
            return accountList;
        } catch (Exception e) {
            throw new DAOException(e.getMessage());
        }
    }

}
