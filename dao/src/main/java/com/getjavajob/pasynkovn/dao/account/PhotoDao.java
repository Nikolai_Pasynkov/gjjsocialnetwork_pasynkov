package com.getjavajob.pasynkovn.dao.account;

import com.getjavajob.pasynkovn.common.account.Photo;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

@Repository
public class PhotoDao {
    private EntityManager entityManager;

    @PersistenceContext(type = PersistenceContextType.EXTENDED)
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void saveOrUpdate(Photo photo) {
        entityManager.merge(photo);
    }

    public Photo getPhoto(long id) {
        return entityManager.find(Photo.class, id);
    }
}
