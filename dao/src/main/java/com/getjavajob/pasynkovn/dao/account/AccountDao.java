package com.getjavajob.pasynkovn.dao.account;

import com.getjavajob.pasynkovn.dao.exceptions.DAOException;
import com.getjavajob.pasynkovn.common.account.Account;

import java.util.List;

public interface AccountDao {
    void add(Account account) throws DAOException;
    Account getById(long id) throws DAOException;
    void update(Account account) throws DAOException;
    void delete(long id) throws DAOException;
    List<Account> getAll() throws DAOException;
    Account getByEmailAndPassword(String email, String password) throws DAOException;
}
