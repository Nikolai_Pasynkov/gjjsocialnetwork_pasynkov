package com.getjavajob.pasynkovn.common.account;

import javax.persistence.*;

@Entity
@Table(name = "photo")
public class Photo {

    @Id
    private long id;

    @Lob
    @Column(name = "bytes")
    private byte[] bytes;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

}
