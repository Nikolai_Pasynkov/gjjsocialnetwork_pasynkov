package com.getjavajob.pasynkovn.common.account;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "accounts")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    @Column(name = "SURNAME")
    private String surName;
    @Column(name = "MIDDLE_NAME")
    private String middleName;
    @Temporal(TemporalType.DATE)
    @Column(name="birthday")
    private java.util.Date birthday;
    private String email;
    private String skype;
    private String icq;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "account", orphanRemoval = true)
    private List<Phone> phones;
    private String password;
    @Enumerated(EnumType.STRING)
    private Role role;
    @Column(name = "REGDATE")
    private Date date;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "friends",
            joinColumns = @JoinColumn(name = "id_one", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "id_two", referencedColumnName = "id")
    )
    private List<Account> friends;

    public void setId(long id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setIcq(String icq) {
        this.icq = icq;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getEmail() {
        return email;
    }

    public String getIcq() {
        return icq;
    }

    public long getId() {
        return id;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    @JsonIgnore
    public List<Phone> getPhones() {
        return phones;
    }

    public String getSkype() {
        return skype;
    }

    public String getSurName() {
        return surName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public List<Account> getFriends() {
        return friends;
    }

    public void setFriends(List<Account> friends) {
        this.friends = friends;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;

        if (id != account.id) return false;
        if (!name.equals(account.name)) return false;
        if (!surName.equals(account.surName)) return false;
        if (middleName != null ? !middleName.equals(account.middleName) : account.middleName != null) return false;
        if (!email.equals(account.email)) return false;
        if (skype != null ? !skype.equals(account.skype) : account.skype != null) return false;
        if (icq != null ? !icq.equals(account.icq) : account.icq != null) return false;
        if (!password.equals(account.password)) return false;
        return role == account.role;
    }

    public boolean haveFriend(Account account) {
        if (!getFriends().isEmpty()) {
            for (Account a : getFriends()) {
                if (account.equals(a)) {
                    return true;
                }
            }
        }
        return false;
    }
}
