package com.getjavajob.pasynkovn.service;

public class ServiceException extends Exception {
    private static final long serialVersionUID = 1L;

    ServiceException(String msg) {
        super(msg);
    }
}