package com.getjavajob.pasynkovn.service;

import com.getjavajob.pasynkovn.common.account.Account;
import com.getjavajob.pasynkovn.common.account.Phone;
import com.getjavajob.pasynkovn.common.account.Photo;
import com.getjavajob.pasynkovn.dao.account.*;
import com.getjavajob.pasynkovn.dao.exceptions.DAOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class AccountService {

    @Autowired
    private AccountDao accountDao;
    @Autowired
    private PhotoDao photoDao;

    @Transactional
    public void create(Account o) throws ServiceException {
        try {
            accountDao.add(o);
        } catch (DAOException e) {
            e.printStackTrace();
            throw new ServiceException(e.getMessage());
        }
    }

    public Account getByEmail(String email, String password) {
        Account account = null;
        try {
            account = accountDao.getByEmailAndPassword(email, password);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return account;
    }

    public Account read(long id) {
        Account account = null;
        try {
            account = accountDao.getById(id);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return account;
    }

    @Transactional
    public void update(Account o) throws ServiceException {
        try {
            findPhonesToDelete(o);
            accountDao.update(o);
        } catch (DAOException e) {
            e.printStackTrace();
            throw new ServiceException(e.getMessage());
        }
    }

    @Transactional
    public void delete(int id) throws ServiceException {
        try {
            accountDao.delete(id);
        } catch (DAOException e) {
            e.printStackTrace();
            throw new ServiceException(e.getMessage());
        }
    }

    public List<Account> readAll() {
        List<Account> accounts = new ArrayList<>();
        try {
            accounts = accountDao.getAll();
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return accounts;
    }

    @Transactional
    public void addOrUpdate(Photo photo) {
        photoDao.saveOrUpdate(photo);
    }

    public Photo getPhoto(long id) {
        return photoDao.getPhoto(id);
    }

    private void findPhonesToDelete(Account account) {
        if (account.getPhones() != null) {
            for (Iterator<Phone> i = account.getPhones().iterator(); i.hasNext();) {
                Phone phone = i.next();
                if (phone.getRemove() == 1) {
                    i.remove();
                }
            }
        }
    }
}
