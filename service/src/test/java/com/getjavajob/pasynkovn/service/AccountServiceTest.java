package com.getjavajob.pasynkovn.service;

import com.getjavajob.pasynkovn.common.account.Account;
import com.getjavajob.pasynkovn.dao.account.AccountDao;
import com.getjavajob.pasynkovn.dao.exceptions.DAOException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class AccountServiceTest {

    @InjectMocks
    private AccountService accountService;
    @Mock
    private AccountDao accountDao;

    @Before
    public  void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void createTest() throws DAOException {
        Account account = new Account();
        account.setId(1);
        try {
            accountService.create(account);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        verify(accountDao).add(any(Account.class));
    }

    @Test
    public void getTest() {
        Account account = new Account();
        account.setId(777);
        account.setName("MOCKITO");
        account.setSurName("MOCKITO");
        account.setEmail("MOCKITO");
        account.setPassword("MOCKITO");
        try {
            when(accountDao.getByEmailAndPassword("MOCKITO", "MOCKITO")).thenReturn(account);
            when(accountDao.getById(777)).thenReturn(account);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        assertEquals(account, accountService.getByEmail("MOCKITO", "MOCKITO"));
        assertEquals(account, accountService.read(777));
    }

    @Test
    public void deleteTest() throws DAOException {
        try {
            accountService.delete(1);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        verify(accountDao).delete(anyLong());
    }
}