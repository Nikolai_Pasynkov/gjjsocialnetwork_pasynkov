**Social Network** 

**Functionality:** 

+ registration  
+ authentication  
+ ajax search  
+ display profile  
+ display friends with pagination  
+ edit profile  
+ upload and download avatar  
 
**Tools:**  
JDK 7, Spring 4, JPA 2 / Hibernate 5, XStream, jQuery 2, Twitter Bootstrap 3, JUnit 4, Mockito, Maven 3, Git / Bitbucket, Tomcat 7, MySQL, IntelliJIDEA 16.  
 
**Screenshots:**  
Start page  
![Alt text](/webapp/src/main/webapp/resources/screenshots/startPage.png?raw=true "Start page")  
  
Account page  
![Alt text](/webapp/src/main/webapp/resources/screenshots/accPage.png?raw=true "Account page")  
  
Edit account  
![Alt text](/webapp/src/main/webapp/resources/screenshots/editPage.png?raw=true "Edit account")  
  
Display friends  
![Alt text](/webapp/src/main/webapp/resources/screenshots/friendList.png?raw=true "Display friends")  
  
_  
**Pasynkov Nikolai**  
Training getJavaJob  
http://www.getjavajob.com